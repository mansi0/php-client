<html>
<head>
<title>Track Order</title>
<link rel="stylesheet" href="./css/css/trackorder.css">
<body>
    <div class="progress">
        <ul>

            <li>
                <img src="./projectphoto/details.jpg"><br>
                <i class="fas fa-check"></i>
                <p>Fill Details</p>
            </li>
            <li>
                <img src="./projectphoto/search.jpg"><br>
                <i class="fas fa-check"></i>
                <p>Select Item</p>
            </li>
            <li>
                <img src="./projectphoto/cart.jpg"><br>
                <i class="fas fa-sync-alt"></i>
                <p>Add to cart</p>
            </li>
           <li>
                <img src="./projectphoto/payment.jpg"><br>
                <i class="fas fa-times"></i>
                <p>Make Payment</p>
            </li>
            <li>
                <img src="./projectphoto/ebook.jpg"><br>
                <i class="fas fa-times"></i>
                <p>Download ebook</p>            

            </li>
        </ul>
    </div>

</body>
</html>