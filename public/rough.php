<body>
    <div class="main">
    <section class="header">
        <div class="content-box">
         <div class="menu">
               <!-- <img src="./projectphoto/logo.png">-->
            <ul>
                <li> <a href="login.html"><i class="fas fa-sign-in-alt"></i>Login</a></li>
                <li> <a href="reg1.html"><i class="fas fa-user-plus"></i>Register</a></li>
            </ul>
         </div>
        <div class="banner-text">
            <h2 >Taste On Way...</h2>
            <!--<a href="#">About us</a>-->
        </div>     
        <!--<slider>
              <slide><p>Slide 1</p></slide>
              <slide><p>Slide 2</p></slide>
              <slide><p>Slide 3</p></slide>
              <slide><p>Slide 4</p></slide>
        </slider>-->
        </div>

    </section>

    <!--service-->
    <section class="service">
        <div class="content-box">
            <div class="container1">
                <div class="box">
                 <div class="content">
                 <h1>01</h1>
                 <h2>Join Our Business</h2>
                 <a href="#">Read More</a>
                 </div>
                </div>


             <div class="container1">
                 <div class="box">
                    <div class="content">
                        <h1>02</h1>
                        <h2>Work With Us</h2>
                        <a href="#">Read More</a>
                    </div>
                 </div>
         </div>

    </section>
    </div>

    <section class="features">
        <div class="content-box">
            <div class="container2">
                <div class="card">
                    <div class="imgBx" data-text="Feature1">
                        <img src="./projectphoto/f2.png">
                    </div>
                <div class="content">
                <div>
                    <h3>Feature1</h3>
                    <p>Self-Pickup</p>
                    <a href="#">Read More</a>
                </div>
            </div>
        </div>

            <div class="card">
                <div class="imgBx" data-text="Feature2">
                <img src="./projectphoto/f2.png">
            </div>
            <div class="content">
            <div>
                <h3>Feature2</h3>
                <p>Express-Delivery</p>
                <a href="#">Read More</a>
            </div>
         </div>
    </div>

            <div class="card">
            <div class="imgBx" data-text="Feature3">
            <img src="./projectphoto/f2.png">
            </div>
            <div class="content">
            <div>
                <h3>Feature3</h3>
                <p>Items are categorized.</p>
                <a href="#">Read More</a>
            </div>
        </div>
    </div>

            <div class="card">
            <div class="imgBx" data-text="Feature4">
             <img src="./projectphoto/f2.png">
            </div>
            <div class="content">
            <div>
                <h3>Feature4</h3>
                <p>features will be added later</p>
                <a href="#">Read More</a>
            </div>
        </div>
    </div>

</div>
</section>
</div>


    <script>
        $(".main").tiltedpage_scroll({
            sectionContainer:"> section",
            angle:50,
            opacity:true,
            scale:true,
            outAnimation:true
        });
    </script>
</body>
</html>
    